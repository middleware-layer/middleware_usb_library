#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: Midleware_USB_Library                                               #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

include(ProjectConf)

###############################
### GENERAL CONFIGURATIONS  ###
###############################

if(USE_COTIRE)
    # We use cotire, simply include it
    coloredMessage(BoldBlue "using cotire" STATUS)
    include(cotire)
else()
    # We do not use cotire, create dummy function.
    coloredMessage(BoldBlue "not using cotire" STATUS)
    function(cotire)
    endfunction(cotire)
endif()

if(USE_BOOST)
    # We use Boost, simply find and include it
    set(Boost_USE_STATIC_LIBS OFF)
    set(Boost_USE_MULTITHREADED ON)
    set(Boost_USE_STATIC_RUNTIME OFF)
    find_package(Boost 1.45.0 REQUIRED)

    if(Boost_FOUND)
        include_directories(${Boost_INCLUDE_DIRS})
        add_executable(progname file1.cxx file2.cxx)
        target_link_libraries(progname ${Boost_LIBRARIES})
    endif()
endif()

if(${STATICLIB_SWITCH} STREQUAL "ON")
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static -Wl,--whole-archive -lpthread -Wl,--no-whole-archive")
  set(CMAKE_FIND_LIBRARY_SUFFIXES ".a;.so")
endif()

#########################################
### GENERAL MIDDLEWARE CONFIGURATIONS ###
#########################################

string(FIND "${EXTERNAL_DEFINES}" "MIDDLEWARE_USE_STM32F7" retVal1)
if(${retVal1} EQUAL -1)
    set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -D${DEFAULT_MIDDLEWARE}")
endif()

###############################
### USB HOST CONFIGURATIONS ###
###############################

string(FIND "${EXTERNAL_DEFINES}" "USBH_MAX_NUM_ENDPOINTS" retVal)
if(retVal EQUAL -1)
    set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -DUSBH_MAX_NUM_ENDPOINTS=${DEFAULT_USBH_MAX_NUM_ENDPOINTS}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DUSBH_MAX_NUM_ENDPOINTS=${DEFAULT_USBH_MAX_NUM_ENDPOINTS}")
endif()

string(FIND "${EXTERNAL_DEFINES}" "USBH_MAX_NUM_INTERFACES" retVal)
if(retVal EQUAL -1)
    set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -DUSBH_MAX_NUM_INTERFACES=${DEFAULT_USBH_MAX_NUM_INTERFACES}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DUSBH_MAX_NUM_INTERFACES=${DEFAULT_USBH_MAX_NUM_INTERFACES}")
endif()

string(FIND "${EXTERNAL_DEFINES}" "USBH_MAX_NUM_CONFIGURATION" retVal)
if(retVal EQUAL -1)
    set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -DUSBH_MAX_NUM_CONFIGURATION=${DEFAULT_USBH_MAX_NUM_CONFIGURATION}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DUSBH_MAX_NUM_CONFIGURATION=${DEFAULT_USBH_MAX_NUM_CONFIGURATION}")
endif()

string(FIND "${EXTERNAL_DEFINES}" "USBH_MAX_NUM_SUPPORTED_CLASS" retVal)
if(retVal EQUAL -1)
    set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -DUSBH_MAX_NUM_SUPPORTED_CLASS=${DEFAULT_USBH_MAX_NUM_SUPPORTED_CLASS}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DUSBH_MAX_NUM_SUPPORTED_CLASS=${DEFAULT_USBH_MAX_NUM_SUPPORTED_CLASS}")
endif()

string(FIND "${EXTERNAL_DEFINES}" "USBH_KEEP_CFG_DESCRIPTOR" retVal)
if(retVal EQUAL -1)
    set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -DUSBH_KEEP_CFG_DESCRIPTOR=${DEFAULT_USBH_KEEP_CFG_DESCRIPTOR}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DUSBH_KEEP_CFG_DESCRIPTOR=${DEFAULT_USBH_KEEP_CFG_DESCRIPTOR}")
endif()

string(FIND "${EXTERNAL_DEFINES}" "USBH_MAX_DATA_BUFFER" retVal)
if(retVal EQUAL -1)
    set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -DUSBH_MAX_DATA_BUFFER=${DEFAULT_USBH_MAX_DATA_BUFFER}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DUSBH_MAX_DATA_BUFFER=${DEFAULT_USBH_MAX_DATA_BUFFER}")
endif()

string(FIND "${EXTERNAL_DEFINES}" "USBH_MAX_SIZE_CONFIGURATION" retVal)
if(retVal EQUAL -1)
    set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -DUSBH_MAX_SIZE_CONFIGURATION=${DEFAULT_USBH_MAX_SIZE_CONFIGURATION}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DUSBH_MAX_SIZE_CONFIGURATION=${DEFAULT_USBH_MAX_SIZE_CONFIGURATION}")
endif()

string(FIND "${EXTERNAL_DEFINES}" "USBH_DEBUG_LEVEL" retVal)
if(retVal EQUAL -1)
    set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -DUSBH_DEBUG_LEVEL=${DEFAULT_USBH_DEBUG_LEVEL}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DUSBH_DEBUG_LEVEL=${DEFAULT_USBH_DEBUG_LEVEL}")
endif()

string(FIND "${EXTERNAL_DEFINES}" "USBH_USE_OS" retVal)
if(retVal EQUAL -1)
    set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -DUSBH_USE_OS=${DEFAULT_USBH_USE_OS}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DUSBH_USE_OS=${DEFAULT_USBH_USE_OS}")
endif()

#################################
### USB DEVICE CONFIGURATIONS ###
#################################



###############################
###  BOARD CONFIGURATIONS   ###
###############################

string(FIND "${EXTERNAL_DEFINES}" "USE_ADAFRUIT_SHIELD" retVal1)
string(FIND "${EXTERNAL_DEFINES}" "USE_STM32F7xx_NUCLEO_144" retVal2)
string(FIND "${EXTERNAL_DEFINES}" "USE_STM32F769I_DISCO" retVal3)
string(FIND "${EXTERNAL_DEFINES}" "USE_STM32F769I_EVAL" retVal4)

if((${retVal1} EQUAL -1) AND (${retVal2} EQUAL -1) AND (${retVal3} EQUAL -1) AND (${retVal4} EQUAL -1))
    set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -D${DEFAULT_Board_Platform}")
endif()

###########################################
###          CMSIS CONFIGURATIONS       ###
###########################################

string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM7" retVal1)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM4" retVal2)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM3" retVal3)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM0" retVal4)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM0PLUS" retVal5)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_ARMV8MBL" retVal6)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_ARMV8MML" retVal7)

if((${retVal1} EQUAL -1) AND (${retVal2} EQUAL -1) AND (${retVal3} EQUAL -1) AND (${retVal4} EQUAL -1) AND
   (${retVal5} EQUAL -1) AND (${retVal6} EQUAL -1) AND (${retVal7} EQUAL -1))
    set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -D${DEFAULT_Building_Library}")
endif()

string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM7" retVal1)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM4" retVal2)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM3" retVal3)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM0" retVal4)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM0PLUS" retVal5)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_ARMV8MBL" retVal6)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_ARMV8MML" retVal7)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM7" retVal8)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM4" retVal9)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM3" retVal10)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM0" retVal11)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM0PLUS" retVal12)

if((${retVal1} EQUAL -1) AND (${retVal2} EQUAL -1) AND (${retVal3} EQUAL -1) AND (${retVal4} EQUAL -1) AND
   (${retVal5} EQUAL -1) AND (${retVal6} EQUAL -1) AND (${retVal7} EQUAL -1) AND (${retVal8} EQUAL -1) AND
   (${retVal9} EQUAL -1) AND (${retVal10} EQUAL -1) AND (${retVal11} EQUAL -1) AND (${retVal12} EQUAL -1))
    set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -D${DEFAULT_Processor_Series}")
endif()

#############################################
###          HAL CONFIGURATIONS           ###
#############################################

string(FIND "${EXTERNAL_DEFINES}" "USE_HAL_DRIVER" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_USE_HAL_DRIVER} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DUSE_HAL_DRIVER=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DUSE_HAL_DRIVER=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "USE_FULL_LL_DRIVER" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_USE_FULL_LL_DRIVER} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DUSE_FULL_LL_DRIVER=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DUSE_FULL_LL_DRIVER=0")
    endif()
endif()

