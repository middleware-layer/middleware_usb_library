/**

 ******************************************************************************
 * @file    USB_Device/MSC_Standalone/Inc/usbd_storage.h
 * @author  MCD Application Team
 * @version V1.2.1
 * @date    14-April-2017
 * @brief   Header for usbd_storage.c module
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __USBD_STORAGE_H_
#define __USBD_STORAGE_H_

/* Includes ------------------------------------------------------------------*/

#include "usbd_msc.h"

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions ------------------------------------------------------- */

extern USBD_StorageTypeDef USBD_DISK_fops;

#endif /* __USBD_STORAGE_H_ */
