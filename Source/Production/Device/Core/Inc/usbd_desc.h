/**
 ******************************************************************************
 * @file    usbd_desc.h
 * @author  MCD Application Team
 * @version V1.2.1
 * @date    14-April-2017
 * @brief   Header for usbd_desc.c module
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __USBD_DESC_H
#define __USBD_DESC_H

/* Includes ------------------------------------------------------------------*/

#include "usbd_def.h"

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

#define         DEVICE_ID1          (0x1FFF7A10)
#define         DEVICE_ID2          (0x1FFF7A14)
#define         DEVICE_ID3          (0x1FFF7A18)

#define  USB_SIZ_STRING_SERIAL       0x1A

/* Exported macro ------------------------------------------------------------*/

/* Exported functions ------------------------------------------------------- */

extern USBD_DescriptorsTypeDef MSC_Desc;

#endif /* __USBD_DESC_H */
