/**
 ******************************************************************************
 * @file    MSCDevice_utilities.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    2 de ago de 2017
 * @brief
 ******************************************************************************
 */

#include <Production/Device/MSCDevice_utilities.hpp>

namespace USB
{
   MSCDevice::MSCDevice(USBD_HandleTypeDef *device)
   {
      // Init Device Library
      USBD_Init(device,
                &MSC_Desc,
                0);

      // Add Supported Class
      USBD_RegisterClass(device,
                         USBD_MSC_CLASS);

      // Add Storage callbacks for MSC Class
      USBD_MSC_RegisterStorage(device,
                               &USBD_DISK_fops);

      // Start Device Process
      USBD_Start(device);
   }

   MSCDevice::~MSCDevice()
   {
      // TODO Auto-generated destructor stub
   }
}
