/**
 ******************************************************************************
 * @file    MSCDevice_utilities.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    2 de ago de 2017
 * @brief
 ******************************************************************************
 */

#ifndef PRODUCTION_DEVICE_MSCDEVICE_UTILITIES_HPP
#define PRODUCTION_DEVICE_MSCDEVICE_UTILITIES_HPP

#include <Production/Device/Core/Inc/usbd_core.h>
#include <Production/Device/Core/Inc/usbd_desc.h>
#include <Production/Device/Class/MSC/Inc/usbd_msc.h>
#include <Production/Device/Class/MSC/Inc/usbd_msc_storage.h>

namespace USB
{
   /**
    * @brief
    */
   class MSCDevice
   {
      private:

      protected:

      public:
         /**
          * @brief
          * @param device :
          */
         MSCDevice(USBD_HandleTypeDef *device);

         /**
          * @brief
          */
         virtual ~MSCDevice();
   };
}

#endif // PRODUCTION_DEVICE_MSCDEVICE_UTILITIES_HPP
