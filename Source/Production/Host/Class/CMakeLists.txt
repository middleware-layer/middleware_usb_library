#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: Midleware_USB_Library                                               #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
# C. Invoke Subdirectories                  #
# D. Libraries / Executables                #
#############################################

coloredMessage(BoldYellow "Loading Host Class CMakeLists" STATUS)

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
#############################################

set(DIRECTORY_NAME_SRC "${PROJECT_PRODUCTION_SOURCE_FOLDER}/Host/Class")
set(DIRECTORY_NAME_INC "${PROJECT_PRODUCTION_SOURCE_FOLDER}/Host/Class")

add_subdirectory(MSC)

#############################################
#   A.1. Set Options                        #
#############################################

set(Host_Class_Sources  ${Host_MSC_Sources}
                        PARENT_SCOPE)

set(Host_Class_Headers  ${Host_MSC_Headers}
                        PARENT_SCOPE)

#############################################
#   A.2. Use Options                        #
#############################################

#############################################
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
#############################################

#############################################
#   B.1. Required libraries                 #
#############################################

#############################################
#   B.2. Optional libraries                 #
#############################################

#############################################
#   B.3. Development and debug libraries    #
#############################################

#############################################
# C. Invoke Subdirectories                  #
#############################################

#############################################
# D. Libraries / Executables                #
#############################################
