/**
 ******************************************************************************
 * @file    usbh_conf_template.h
 * @author  MCD Application Team
 * @version V3.2.2
 * @date    07-July-2015
 * @brief   Header file for usbh_conf_template.c
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __USBH_CONF_H
#define __USBH_CONF_H

#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* CMSIS OS macros */
#if (USBH_USE_OS == 1)
#include <Production/CMSIS_RTOS/cmsis_os.h>
#define   USBH_PROCESS_PRIO    osPriorityNormal
#endif

/* Memory management macros */
#define USBH_malloc               malloc
#define USBH_free                 free
#define USBH_memset               memset
#define USBH_memcpy               memcpy

/* DEBUG macros */
#if (USBH_DEBUG_LEVEL > 0)
#define USBH_UsrLog(...)   printf(__VA_ARGS__);\
							   printf("\n");
#else
#define USBH_UsrLog(...)
#endif

#if (USBH_DEBUG_LEVEL > 1)
#define USBH_ErrLog(...)   printf("ERROR: ") ;\
							   printf(__VA_ARGS__);\
							   printf("\n");
#else
#define USBH_ErrLog(...)
#endif

#if (USBH_DEBUG_LEVEL > 2)
#define USBH_DbgLog(...)   printf("DEBUG : ") ;\
							   printf(__VA_ARGS__);\
							   printf("\n");
#else
#define USBH_DbgLog(...)
#endif

#ifdef __cplusplus
}
#endif

/* Exported functions ------------------------------------------------------- */

#endif /* __USBH_CONF_H */
