/**
 ******************************************************************************
 * @file    usbh_ioreq.h
 * @author  MCD Application Team
 * @version V3.2.2
 * @date    07-July-2015
 * @brief   Header file for usbh_ioreq.c
 ******************************************************************************
 */

/* Define to prevent recursive  ----------------------------------------------*/

#ifndef __USBH_IOREQ_H
#define __USBH_IOREQ_H

#ifdef __cplusplus
extern "C"
{
#endif

   /* Includes ------------------------------------------------------------------*/

#include "usbh_conf.h"
#include "usbh_core.h"

#define USBH_PID_SETUP                            0
#define USBH_PID_DATA                             1

#define USBH_EP_CONTROL                           0
#define USBH_EP_ISO                               1
#define USBH_EP_BULK                              2
#define USBH_EP_INTERRUPT                         3

#define USBH_SETUP_PKT_SIZE                       8

   USBH_StatusTypeDef USBH_CtlSendSetup(USBH_HandleTypeDef *phost,
                                        uint8_t *buff,
                                        uint8_t hc_num);

   USBH_StatusTypeDef USBH_CtlSendData(USBH_HandleTypeDef *phost,
                                       uint8_t *buff,
                                       uint16_t length,
                                       uint8_t hc_num,
                                       uint8_t do_ping);

   USBH_StatusTypeDef USBH_CtlReceiveData(USBH_HandleTypeDef *phost,
                                          uint8_t *buff,
                                          uint16_t length,
                                          uint8_t hc_num);

   USBH_StatusTypeDef USBH_BulkReceiveData(USBH_HandleTypeDef *phost,
                                           uint8_t *buff,
                                           uint16_t length,
                                           uint8_t hc_num);

   USBH_StatusTypeDef USBH_BulkSendData(USBH_HandleTypeDef *phost,
                                        uint8_t *buff,
                                        uint16_t length,
                                        uint8_t hc_num,
                                        uint8_t do_ping);

   USBH_StatusTypeDef USBH_InterruptReceiveData(USBH_HandleTypeDef *phost,
                                                uint8_t *buff,
                                                uint8_t length,
                                                uint8_t hc_num);

   USBH_StatusTypeDef USBH_InterruptSendData(USBH_HandleTypeDef *phost,
                                             uint8_t *buff,
                                             uint8_t length,
                                             uint8_t hc_num);

   USBH_StatusTypeDef USBH_IsocReceiveData(USBH_HandleTypeDef *phost,
                                           uint8_t *buff,
                                           uint32_t length,
                                           uint8_t hc_num);

   USBH_StatusTypeDef USBH_IsocSendData(USBH_HandleTypeDef *phost,
                                        uint8_t *buff,
                                        uint32_t length,
                                        uint8_t hc_num);

#ifdef __cplusplus
}
#endif

#endif /* __USBH_IOREQ_H */
