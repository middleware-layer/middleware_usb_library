/**
 ******************************************************************************
 * @file    MSCHost_utilities.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    3 de ago de 2017
 * @brief
 ******************************************************************************
 */

#include <Production/Host/MSCHost_utilities.hpp>

namespace USB
{
   MSCHost::MSCHost(USBH_HandleTypeDef *hUSBHost)
   {
      Appli_state = MSC_ApplicationTypeDef::APPLICATION_IDLE;

      /*
       void (*functionPtr)(USBH_HandleTypeDef *phost,
                          uint8_t id) = &MSCHost::userProcess;

      // Init Host Library
      USBH_Init(hUSBHost,
                functionPtr,
                0);

      // Add Supported Class
      USBH_RegisterClass(hUSBHost,
                         USBH_MSC_CLASS);

      // Start Host Process
      USBH_Start(hUSBHost);
      */
   }

   MSCHost::~MSCHost()
   {
      // TODO Auto-generated destructor stub
   }

   auto MSCHost::userProcess(USBH_HandleTypeDef *phost,
                             uint8_t id) -> void
   {
      switch (id)
      {
         case HOST_USER_SELECT_CONFIGURATION:

            break;

         case HOST_USER_DISCONNECTION:
            Appli_state = MSC_ApplicationTypeDef::APPLICATION_DISCONNECT;

//				if(f_mount(NULL, "", 0) != FR_OK)
//				{
//
//				}

            break;

         case HOST_USER_CLASS_ACTIVE:
            Appli_state = MSC_ApplicationTypeDef::APPLICATION_READY;

            break;

         case HOST_USER_CONNECTION:
//				if(f_mount(&fatfs, "", 0) == FR_OK)
//				{
//
//				}

            break;

         default:
            break;
      }
   }

}
