/**
 ******************************************************************************
 * @file    MSCHost_utilities.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    3 de ago de 2017
 * @brief
 ******************************************************************************
 */

#ifndef PRODUCTION_HOST_MSCHOST_UTILITIES_HPP
#define PRODUCTION_HOST_MSCHOST_UTILITIES_HPP

#include <Production/Host/Class/MSC/Inc/usbh_msc.h>
#include <Production/Host/Core/Inc/usbh_core.h>

namespace USB
{
   /**
    * @brief
    */
   class MSCHost
   {
      private:
         enum class MSC_ApplicationTypeDef
         {
            APPLICATION_IDLE = 0,
            APPLICATION_READY,
            APPLICATION_DISCONNECT,
         };

         /**
          * @brief
          */
         MSC_ApplicationTypeDef Appli_state;

         /**
          * @brief  User Process
          * @param  phost: Host Handle
          * @param  id: Host Library user message ID
          */
         auto userProcess(USBH_HandleTypeDef *phost,
                          uint8_t id) -> void;

      protected:

      public:
         /**
          * @brief
          * @param  hUSBHost:
          */
         MSCHost(USBH_HandleTypeDef *hUSBHost);

         /**
          * @brief
          */
         virtual ~MSCHost();
   };
}

#endif // PRODUCTION_HOST_MSCHOST_UTILITIES_HPP
