/**
 ******************************************************************************
 * @file    MSCDevice_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/Device/MSCDevice_utilities_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace USB
{
   static I_MSCDevice* p_MSCDevice_Impl = NULL;

   const char* MSCDevice_StubImplNotSetException::what(void) const throw()
   {
      return "No stub implementation is set to handle MSCDevice functions";
   }

   void stub_setImpl(I_MSCDevice* pNewImpl)
   {
      p_MSCDevice_Impl = pNewImpl;
   }

   static I_MSCDevice* MSCDevice_Stub_Get_Impl(void)
   {
      if(p_MSCDevice_Impl == NULL)
      {
         std::cerr << "ERROR: No Stub implementation is currently set to handle "
                   << "calls to MSCDevice functions. Did you forget"
                   << "to call Stubs::USB::stub_setImpl() ?" << std::endl;

         throw MSCDevice_StubImplNotSetException();
      }

      return p_MSCDevice_Impl;
   }

      ////////////////////////////////////
      // I_MSCDevice Methods Definition //
      ////////////////////////////////////

   I_MSCDevice::~I_MSCDevice()
   {
      if(p_MSCDevice_Impl == this)
      {
         p_MSCDevice_Impl = NULL;
      }
   }

   ////////////////////////////////////////
   // _Mock_MSCDevice Methods Definition //
   ////////////////////////////////////////

   _Mock_MSCDevice::_Mock_MSCDevice()
   {

   }

       //////////////////////////////////
       // Definition of Non Stub Class //
       //////////////////////////////////

   MSCDevice::MSCDevice(USBD_HandleTypeDef *device)
   {
   }

   MSCDevice::~MSCDevice()
   {
   }
}

