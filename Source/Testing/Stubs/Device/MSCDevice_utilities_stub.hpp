/**
 ******************************************************************************
 * @file    MSCDevice_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_DEVICE_MSCDEVICE_UTILITIES_STUB_HPP
#define SOURCE_TESTING_STUBS_DEVICE_MSCDEVICE_UTILITIES_STUB_HPP

#include <Production/Device/MSCDevice_utilities.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace USB
{
   class MSCDevice_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_MSCDevice
   {
      public:
         virtual ~I_MSCDevice();
   };

   class _Mock_MSCDevice : public I_MSCDevice
   {
      public:
         _Mock_MSCDevice();
   };

   typedef ::testing::NiceMock<_Mock_MSCDevice> Mock_MSCDevice;

   void stub_setImpl(I_MSCDevice* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_DEVICE_MSCDEVICE_UTILITIES_STUB_HPP
